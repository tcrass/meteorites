extends Node
class_name Main

const DEADZONE = 0.2

const RESET = 0
const STARTED = 1
const STOPPED = 2

export var alert_signals_count : int = 5
export var alert_signal_duration_ms : float = 500

signal state_changed(state);

var running : bool = false
var _alert_signals_count : float = 0
var _alert_signal_on : bool = false setget _set_alert_signal_on

func _set_alert_signal_on(value):
    if value != _alert_signal_on:
        _alert_signal_on = value
        if _alert_signal_on:
            _alert_signals_count += 1
        _update_alert()
        
func _ready():
    InputMap.action_set_deadzone("left", DEADZONE)
    InputMap.action_set_deadzone("right", DEADZONE)
    InputMap.action_set_deadzone("up", DEADZONE)
    InputMap.action_set_deadzone("down", DEADZONE)
    find_node("Space").connect("score_changed", self, "_on_score_changed")
    find_node("Space").connect("game_over", self, "_on_game_over")
    $Timer.connect("timeout", self, "_on_timeout")

    reset()

func _input(event : InputEvent):
    if event.is_action("ui_accept") and not running:
        start()
    if event.is_action("ui_cancel"):
        get_tree().quit()        
    
func reset():
    stop()
    $Timer.stop()
    _alert_signal_on = false
    _alert_signals_count = 0
    emit_signal("state_changed", RESET)
    
func start():
    if not running:
        reset()
        running = true
        self._alert_signal_on = true
        $"CockpitLayer/CollisionsDisplay".text = str(0)
        $"CockpitLayer/PassesDisplay".text = str(0)
        $Timer.wait_time = alert_signal_duration_ms / 1000
        $Timer.start()
        emit_signal("state_changed", STARTED)

func stop():
    if running:
        running = false
        $Timer.stop()
        self._alert_signal_on = false
        $AlertOffSound.play()
        emit_signal("state_changed", STOPPED)

func _on_score_changed(collisions_count : float, passed_count : float):
    $"CockpitLayer/CollisionsDisplay".text = str(collisions_count)
    $"CockpitLayer/PassesDisplay".text = str(passed_count)
    
func _on_game_over():
    stop()

func _on_timeout():
    self._alert_signal_on = not _alert_signal_on
    if running:
        $Timer.start()
    
func _update_alert():
    $"CockpitLayer/AlertOn".visible = _alert_signal_on
    if _alert_signal_on:
        if _alert_signals_count <= alert_signals_count:
            $AlertSound.play()
    else:
        $AlertSound.stop()
    