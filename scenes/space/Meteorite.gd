extends Spatial
class_name Meteorite

signal collided(meteorite)
signal passed(meteorite)

var velocity : Vector3 = Vector3(0, 0, 0)
var rotation_axis : Vector3 = Vector3.UP
var rotation_speed : float = 0

func _ready():
    $Object.connect("area_entered", self, "_on_collision")

func _physics_process(delta):
    $Object.rotate(rotation_axis, delta*rotation_speed)

    var lateral_ship_velocity = $"../Ship".lateral_velocity()
    var v = velocity - Vector3(lateral_ship_velocity.x, lateral_ship_velocity.y, 0)
    translate(delta * v)

    if transform.origin.z > 0:
        emit_signal("passed", self)
        self.queue_free()

func _on_collision(body : Node):
    emit_signal("collided", self)
    self.queue_free()