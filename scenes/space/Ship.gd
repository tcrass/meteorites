extends Node

signal direction_changed()


export var ship_width : float = 4
export var ship_height : float = 6
export var ship_velocity : float = 20
export var max_theta : float = 30.0
export var max_phi : float = 30.0
export var direction_change_duration_ms : float = 100
export var max_role_angle : float = 15

var target_theta : float = 0 setget _set_target_theta
var target_phi : float = 0 setget _set_target_phi

var theta : float = 0 setget _set_theta
var phi : float = 0 setget _set_phi

func _set_target_theta(value : float):
    if value != target_theta:
        target_theta = value
        $ShipTween.stop(self, "theta")
        $ShipTween.interpolate_property(self, "theta", theta, target_theta, direction_change_duration_ms/1000, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
        $ShipTween.start()

func _set_target_phi(value : float):
    if value != target_phi:
        target_phi = value
        $ShipTween.stop(self, "phi")
        $ShipTween.interpolate_property(self, "phi", phi, target_phi, direction_change_duration_ms/1000, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
        $ShipTween.start()

func _set_theta(value : float):
    theta = value
    _update_engine_sound()
    emit_signal("direction_changed")

func _set_phi(value : float):
    phi = value
    _update_engine_sound()
    emit_signal("direction_changed")

func set_immediate_theta(value : float):
    $ShipTween.stop(self)
    if value != theta:
        theta = value
        emit_signal("direction_changed")
        
func set_immediate_phi(value : float):
    $ShipTween.stop(self)
    if value != phi:
        phi = value
        emit_signal("direction_changed")

func theta_rad() -> float:
    return deg2rad(theta)        

func phi_rad() -> float:
    return deg2rad(phi)        

func velocity() -> Vector3:
    return Vector3(0, 0, -ship_velocity).rotated(Vector3.RIGHT, phi_rad()).rotated(Vector3.UP, -theta_rad())

func lateral_velocity() -> Vector2:
    var v = velocity()
    return Vector2(v.x, v.y)

func _ready():
    $Object.scale = Vector3(ship_width, ship_height, 1)
    
func on_state_changed(state : int):
    if state == Main.RESET:
        reset()
    elif state == Main.STARTED:
        start()
    elif state == Main.STOPPED:
        stop()

func reset():
    target_theta = 0
    target_phi = 0
    emit_signal("direction_changed")

func start():
    reset()
    
func stop():
    pass

func _update_engine_sound():
    var max_vol = Vector2(max_theta, max_phi).length()
    var rel_vol = lateral_velocity().length()/max_vol
    if rel_vol > 0:
        $EngineSound.volume_db = -30*(1 - rel_vol)
        if not $EngineSound.playing:
            $EngineSound.play()
    else:
        $EngineSound.stop()
        