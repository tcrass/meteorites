extends Node

const METEORITE_PROTO = preload("res://scenes/space/Meteorite.tscn")

export var meteorites_per_game_count : int = 1000

export var meteorite_min_scale : float = 0.3
export var meteorite_max_scale : float = 6
export var meteorite_max_scale_deviation_factor : float = 1
export var meteorite_max_roration_speed : float = 120

export var meteorite_velocity : float = 40
export var emission_distance : float = 300
export var emission_delay_ms : float = 25
export var impact_probability : float = 0.2
export var h_distr_angle : float = 60
export var v_distr_angle : float = 22.5

var _emitted_count : int  = 0
var _running : bool = false

func _ready():
    $EmissionTimer.connect("timeout", self, "_on_emission_timeout")

func on_state_changed(state : int):
    if state == Main.RESET:
        reset()
    elif state == Main.STARTED:
        start()
    elif state == Main.STOPPED:
        stop()
    
func reset():
    $EmissionTimer.stop()
    _emitted_count = 0
        
func start():
    reset()
    _running = true
    _emit()
    
func stop():
    _running = false
    $EmissionTimer.stop()
    
func _emit():
    if _running && _emitted_count < meteorites_per_game_count:
        var meteorite = METEORITE_PROTO.instance()
        var object = meteorite.get_node("Object")
        
        var scale_x = rand_range(meteorite_min_scale, meteorite_max_scale)
        var scale_y = scale_x * rand_range(1 + meteorite_max_scale_deviation_factor, 1/(1 + meteorite_max_scale_deviation_factor))
        var scale_z = scale_x * rand_range(1 + meteorite_max_scale_deviation_factor, 1/(1 + meteorite_max_scale_deviation_factor))
        object.scale = Vector3(scale_x, scale_y, scale_z) 
        object.rotation = Vector3(randf()*2*PI, randf()*2*PI, randf()*2*PI)
        var original_size = object.get_node("MeshInstance").get_aabb().size.x
        var size = max(scale_x * original_size, max(scale_y * original_size, scale_z * original_size))
    
        var lateral_ship_velocity = $"../Ship".lateral_velocity()
    
        var time_to_impact = emission_distance / meteorite_velocity
        var emission_point = Vector3(0, 0, -emission_distance) + time_to_impact * Vector3(lateral_ship_velocity.x, lateral_ship_velocity.y, 0)
        meteorite.translate(emission_point)
        
        var lateral_meteorite_velocity = Vector3(0, 0, 0)
        if randf() > impact_probability:
            lateral_meteorite_velocity =  meteorite_velocity * Vector3(tan(deg2rad(rand_range(-h_distr_angle, h_distr_angle))), tan(deg2rad(rand_range(-v_distr_angle, v_distr_angle))), 0)
        else:        
            lateral_meteorite_velocity =  (size/2 + $"../Ship".ship_width)/time_to_impact * randf() * (Vector3.RIGHT.rotated(Vector3.FORWARD, randf()*2*PI))
        meteorite.velocity = Vector3(0, 0, meteorite_velocity) + lateral_meteorite_velocity
        meteorite.rotation_speed = deg2rad(rand_range(-meteorite_max_roration_speed, meteorite_max_roration_speed))
  
        meteorite.connect("collided", get_parent(), "on_collision")
        meteorite.connect("passed", get_parent(), "on_pass")
        get_parent().add_child(meteorite)
        _emitted_count += 1
        $EmissionTimer.wait_time = emission_delay_ms / 1000
        $EmissionTimer.start()
    
func _on_emission_timeout():
    _emit()
  