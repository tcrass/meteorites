extends Spatial
class_name Space

signal state_changed(state);
signal score_changed(collisions_count, passes_count)
signal game_over

var collisions_count : int = 0
var passes_count : int = 0

export var camera_sensitivity : float = 0.4
export var camera_wobble_duration_ms : float = 500
export var camera_wobble_strength : float = 0.5
export var sky_rotation_speed_factor : float = 0.02

var _camera_wobble_progress : float = 1 setget _set_camera_wobble_progress
var _camera_wobble_direction : Vector3 = Vector3.ZERO
var _camera_wobble : Vector3 = Vector3.ZERO setget _set_camera_wobble
var _sky_direction : Vector3 = Vector3.FORWARD
var _sky_up : Vector3 = Vector3.UP

func _set_camera_wobble_progress(value : float):
    if value != _camera_wobble_progress:
        _camera_wobble_progress = value
        self._camera_wobble = (1 - _camera_wobble_progress) * _camera_wobble_direction
        
func _set_camera_wobble(value : Vector3):
    if value != _camera_wobble:
        _camera_wobble = value
        _update_view()

# Called when the node enters the scene tree for the first time.
func _ready():
    $WorldEnvironment.environment.background_sky.panorama.load("res://assets/space_panorama.png")
    $"/root/Main".connect("state_changed", self, "_on_state_changed")
    connect("state_changed", $MeteoriteController, "on_state_changed")
    connect("state_changed", $Ship, "on_state_changed")
    $Ship.connect("direction_changed", self, "_on_ship_direction_changed")

func _process(delta):
    if $Ship.theta != 0 or $Ship.phi != 0:
        var sky_rotation_axis = -Vector3.FORWARD.cross($Ship.velocity()).normalized()
        var sky_rotation_amount = delta * sky_rotation_speed_factor * $Ship.lateral_velocity().length()
        _sky_direction = _sky_direction.rotated(sky_rotation_axis, sky_rotation_amount)
        _sky_up = _sky_up.rotated(sky_rotation_axis, sky_rotation_amount)
        _update_view()
    

func _on_state_changed(state : int):
    if state == Main.RESET:
        reset()
    elif state == Main.STARTED:
        start()
    elif state == Main.STOPPED:
        stop()

func reset():
    collisions_count = 0
    passes_count = 0
    $Tween.stop(self)
    self._camera_wobble_progress = 1
    self._camera_wobble = Vector3.ZERO
    emit_signal("state_changed", Main.RESET)
    
func start():
    emit_signal("state_changed", Main.STARTED)
    
func stop():
    emit_signal("state_changed", Main.STOPPED)

func _input(event : InputEvent):
    if event is InputEventKey:
        if event.is_action("ui_left"):
            $Ship.target_theta = -$Ship.max_theta if event.is_pressed() else 0
        elif event.is_action("ui_right"):
            $Ship.target_theta = +$Ship.max_theta if event.is_pressed() else 0
        elif event.is_action("ui_up"):
            $Ship.target_phi = +$Ship.max_phi if event.is_pressed() else 0
        elif event.is_action("ui_down"):
            $Ship.target_phi = -$Ship.max_phi if event.is_pressed() else 0
    elif event is InputEventJoypadMotion:
        if event.is_action("left") or event.is_action("right"):
            var theta = $Ship.max_theta * (event.get_action_strength("right") - event.get_action_strength("left"))
            $Ship.target_theta = theta
            #$Ship.set_immediate_theta(theta)
        if event.is_action("up") or event.is_action("down"):
            var phi = $Ship.max_phi * (event.get_action_strength("down") - event.get_action_strength("up"))
            $Ship.target_phi = phi
            #$Ship.set_immediate_phi(phi)

func _on_ship_direction_changed():
    _update_view()
    
func _update_view():
    $WorldEnvironment.environment.background_sky_orientation = Basis(_sky_direction.cross(_sky_up), _sky_up, -_sky_direction).orthonormalized()
    var look_dir = Vector3.FORWARD.rotated(Vector3.RIGHT, camera_sensitivity * $Ship.phi_rad()).rotated(Vector3.UP, -0.4*$Ship.theta_rad()) + _camera_wobble
    var up_dir = Vector3.UP.rotated(Vector3.FORWARD, deg2rad($Ship.max_role_angle * $Ship.theta/$Ship.max_theta))
    $Camera.look_at(look_dir, up_dir)
        
    
func on_collision(meteorite : Meteorite):
    $CollisionSound.play()
    collisions_count += 1
    _update_score()
    _camera_wobble_direction = camera_wobble_strength * (meteorite.get_global_transform().origin - get_global_transform().origin).normalized()
    $Tween.stop(self)
    $Tween.interpolate_property(self, "_camera_wobble_progress", 0, 1, camera_wobble_duration_ms/1000, Tween.TRANS_ELASTIC, Tween.EASE_OUT, 0)
    $Tween.start()    
    
func on_pass(meteorite : Meteorite):
    passes_count += 1
    _update_score()

func _update_score():
    emit_signal("score_changed", collisions_count, passes_count)
    if collisions_count + passes_count >= $MeteoriteController.meteorites_per_game_count:
        emit_signal("game_over")
        
